import React from 'react';
import PropTypes from 'prop-types';
import PhotoThumbnail from "../PhotoThumbnail/PhotoThumbnail";
import './PhotoListItem.css';
import {Button, Col, Row} from "reactstrap";
import {Link} from "react-router-dom";

const PhotoListItem = props => {
    console.log(props);

    return (

    <div className='CardBody'>
      <div className='Card'>
        <div className='Img'>
          <Link to={`/photos/${props._id}`}>
            <h2>{props.title}</h2>
          </Link>
          <PhotoThumbnail onClick={props.onClick} image={props.image}/>
            <Link to={`/photos/${props.userPhoto._id}`}>
                {props && props.user && <h2>{props.userPhoto.displayName}</h2>}
            </Link>
        </div>
      </div>
        {(props.user && props.userPhoto._id === props.user._id) ? <div>
            <Row>
                <Col sm={4}>
                    {(props.removed === true) ? <h5 style={{color: 'red', textAlign: 'center'}}>
                        <strong>Удалено!</strong>
                    </h5>: null}
                </Col>
                {props.user &&
                <Col sm={2}>
                    <Button
                        style={{margin: '20px'}}
                        color="danger"
                        className="float-right"
                        onClick={() => props.removedPhoto(props._id, {removed: true})}
                    >
                        Remove
                    </Button>
                </Col>
                }
            </Row>
        </div> :null}

    </div>
  );
};

PhotoListItem.propTypes = {
  image: PropTypes.string,
  _id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
};

export default PhotoListItem;
