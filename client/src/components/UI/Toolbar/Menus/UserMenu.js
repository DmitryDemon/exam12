import React from 'react';
import {DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown} from "reactstrap";

const UserMenu = ({user, logout}) => {
    const BASE_URL = 'http://localhost:8000/uploads/';
    const src = user.facebookId ? user.avatarImage : BASE_URL + user.avatarImage;
    return (
        <UncontrolledDropdown nav inNavbar>
            <DropdownToggle nav caret>
                <img width='40px' src={src} alt="userImg"/>
                Hello, {user.username}
            </DropdownToggle>
            <DropdownMenu right>
                <DropdownItem divider/>
                <DropdownItem onClick={logout}>
                    Logout
                </DropdownItem>
            </DropdownMenu>
        </UncontrolledDropdown>
        )

};

export default UserMenu;
