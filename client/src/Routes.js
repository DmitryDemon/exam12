import React from 'react';
import {Route, Switch} from "react-router-dom";

import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";


import Photos from "./containers/Photos/Photos";
import NewPhoto from "./containers/NewPhoto/NewPhoto";



const Routes = () => {
  return (
    <Switch>
      <Route path="/" exact component={Photos}/>
        <Route path="/photos/new" exact component={NewPhoto}/>
        <Route path="/photos/:userId" exact component={Photos}/>
       <Route path="/register" exact component={Register}/>
      <Route path="/login" exact component={Login}/>
    </Switch>
  );
};

export default Routes;