import {FETCH_PHOTO_SUCCESS} from "../actions/photosActions";
import {FETCH_ONE_PHOTO_SUCCESS} from "../actions/onePhotoActions";


const initialState = {
  photos: [],
  onePhoto: null,
};

const photosReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PHOTO_SUCCESS:
      return {...state, photos: action.photos};
    case FETCH_ONE_PHOTO_SUCCESS:
      return {...state, onePhoto: action.photos};
    default:
      return state;
  }
};

export default photosReducer;
