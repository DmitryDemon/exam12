import axios from '../../axios-api';
import {NotificationManager} from "react-notifications";

export const FETCH_PHOTO_SUCCESS = 'FETCH_PHOTO_SUCCESS';
export const CREATE_PHOTO_SUCCESS = 'CREATE_PHOTO_SUCCESS';

export const fetchPhotosSuccess = photos => ({type: FETCH_PHOTO_SUCCESS, photos});
export const createPhotoSuccess = () => ({type: CREATE_PHOTO_SUCCESS});

export const fetchPhotos = userId => {
  return dispatch => {
    let url = '/photos';
    if (userId) {
      url += '?user=' + userId
    }
    return axios.get(url).then(
      response => dispatch(fetchPhotosSuccess(response.data))
    );
  };
};

export const createPhoto = photoData => {
  return dispatch => {
    return axios.post('/photos', photoData).then(
      () => dispatch(createPhotoSuccess())
    );
  };
};

export const removedPhoto = (id, remove) => {
  return dispatch => {
    return axios.post(`/photos/${id}/toggle_removed`, remove).then(
      () => {
        dispatch(fetchPhotos());
        NotificationManager.warning('You removed an cocktail!');
      }
    );
  };
};


