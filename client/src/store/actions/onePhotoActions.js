import axios from '../../axios-api';

export const FETCH_ONE_PHOTO_SUCCESS = 'FETCH_ONE_PHOTO_SUCCESS';

export const fetchOnePhotoSuccess = photos => ({type: FETCH_ONE_PHOTO_SUCCESS, photos});

export const fetchOnePhoto = id => {
  return (dispatch) => {
    return axios.get('/photos/' + id).then(
      response => {
        dispatch(fetchOnePhotoSuccess(response.data));
      }
    );
  };
};