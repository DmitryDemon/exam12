import React, {Component, Fragment} from 'react';
import {Button, Col, Modal, ModalBody, ModalFooter, Row} from "reactstrap";
import {connect} from "react-redux";
import {Link} from "react-router-dom";

import PhotoListItem from "../../components/PhotoListItem/PhotoListItem";
import {fetchPhotos, removedPhoto} from "../../store/actions/photosActions";
import {apiURL} from "../../constants";

class Cocktails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false
        };

        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }

    componentDidMount() {
        this.props.fetchPhoto(this.props.match.params.userId)
    };

    componentDidUpdate(prevProps) {
        if (prevProps.match.params.userId !== this.props.match.params.userId) {
            this.props.fetchPhoto(this.props.match.params.userId);
        }
    };
    render() {
        console.log(this.props.photos);
        return (
            <Row>
                <Col sm={12}>
                    <h2>
                      Cocktails
                      {(this.props.user && this.props.user) ?
                      <Link to="/photos/new">
                          <Button
                              color="primary"
                              className="float-right"
                          >
                              Add Photo
                          </Button>
                      </Link>
                      : null}
                    </h2>
                </Col>
                <Col sm={12}>
                  {this.props.photos.map(photo => {
                      return (
                          <Fragment>
                              <PhotoListItem
                                  key={photo._id}
                                  _id={photo._id}
                                  title={photo.title}
                                  image={photo.image}
                                  removed={photo.removed}
                                  userPhoto={photo.user}
                                  user={this.props.user}
                                  removedPhoto={this.props.removedPhoto}
                                  onClick={this.toggle}
                              />
                              <Modal size={5} isOpen={this.state.modal} toggle={this.toggle}>
                                  <ModalBody>
                                      <img style={{width: '100%'}} src={apiURL + '/uploads/' + photo.image} alt="Gallery"/>
                                  </ModalBody>
                                  <ModalFooter>
                                      <Button color="secondary" onClick={this.toggle}>Close</Button>
                                  </ModalFooter>
                              </Modal>
                          </Fragment>
                          )
                  })}
                </Col>
            </Row>
        )
    }
}

const mapStateToProps = state => ({
    photos: state.photos.photos,
    user: state.users.user,
});

const mapDispatchToProps = dispatch => ({
    fetchPhoto: userId => dispatch(fetchPhotos(userId)),
  removedPhoto: (id, remove) => dispatch(removedPhoto(id, remove)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Cocktails);
//sory