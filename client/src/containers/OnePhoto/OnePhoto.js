import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";


import PhotoThumbnail from "../../components/PhotoThumbnail/PhotoThumbnail";
import '../../components/PhotoListItem/PhotoListItem.css';
import {fetchOnePhoto} from "../../store/actions/onePhotoActions";


class onePhoto extends Component {

    componentDidMount() {
        this.props.onFetchPhoto(this.props.match.params.id);
    }

    render() {
        console.log(this.props.onePhoto);
        return (
            <Fragment>
              {this.props && this.props.onePhoto ?
                <div className='CardBody'>
                  <div className='Card'>
                    <div className='Img'>
                      <h2>{this.props.onePhoto.photo.title}</h2>
                      <PhotoThumbnail image={this.props.onePhoto.photo.image}/>
                    </div>
                  </div>
                </div>: null}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    onePhoto: state.onePhoto.onePhoto,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    onFetchPhoto: (photos) => dispatch(fetchOnePhoto(photos)),
});

export default connect(mapStateToProps, mapDispatchToProps)(onePhoto);
