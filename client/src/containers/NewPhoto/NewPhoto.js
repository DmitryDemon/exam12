import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";

import PhotoForm from "../../components/PhotoForm/PhotoForm";
import {createPhoto} from "../../store/actions/photosActions";


class NewPhoto extends Component {

  createPhoto = photoData => {
    this.props.onPhotoCreated(photoData).then(() => {
      this.props.history.push('/');
    });
  };

  render() {
    return (
      <Fragment>
        <h2>New Photo</h2>
        <PhotoForm
          onSubmit={this.createPhoto}
          photos={this.props.photos}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
    photos: state.photos.photos,
});

const mapDispatchToProps = dispatch => ({
  onPhotoCreated: photoData => dispatch(createPhoto(photoData)),

});

export default connect(mapStateToProps, mapDispatchToProps)(NewPhoto);
