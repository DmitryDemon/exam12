const mongoose = require('mongoose');
const config = require('./config');
const nanoid = require('nanoid');

const User = require('./models/User');
const Photo = require('./models/Photo');

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for (let collection of collections) {
    await collection.drop();
  }

  const user = await User.create(
      {
          username: 'Linda Alcgbhfebchad Smithwitz',
          password: nanoid(),
          token: nanoid(),
          displayName: 'Linda',
          facebookId: '101194507800100',
          avatarImage: 'http://urlid.ru/bq2y'
      },
      {
          username: 'John Week',
          password: '123',
          token: nanoid(),
          displayName: 'Killer',
          avatarImage: 'unnamed.jpg'
      },
      {
          username: 'Ad Haizenberg',
          password: '123',
          token: nanoid(),
          displayName: 'Haizenberg',
          avatarImage: 'admin.jpeg'
      },
  );

  await Photo.create(
        {
          user: user[0]._id,
          title: 'Мохито',
          image: 'mojito.jpg',
        },
      {
          user: user[0]._id,
          title: 'Манхетен',
          image: 'manhattan.png',
      },
      {
          user: user[1]._id,
          title: 'Мезмерайз',
          image: 'mezmerize.jpeg',
      },
      {
          user: user[1]._id,
          title: 'SOAD',
          image: 'soad.jpeg',
      },
      {
          user: user[1]._id,
          title: 'Токсисити',
          image: 'toxicity.jpeg',
      },
      {
          user: user[2]._id,
          title: 'Трубка1',
          image: 'pepe1.jpg',
      },
      {
          user: user[2]._id,
          title: 'Трубка2',
          image: 'pepe2.jpg',
      },
      {
          user: user[2]._id,
          title: 'Трубка3',
          image: 'pepe3.jpg',
      },
      {
          user: user[2]._id,
          title: 'Трубка4',
          image: 'pepe4.jpg',
      },
      {
          user: user[2]._id,
          title: 'Трубка5',
          image: 'pepe5.jpg',
      },



  );

  return connection.close();
};


run().catch(error => {
  console.error('Something wrong happened...', error);
});
