const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');

const auth = require('../middleware/auth');
const tryAuth = require('../middleware/tryAuth');
const Photo = require('../models/Photo');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', tryAuth, async (req, res) => {
    try {
        let criteria = {removed: false};

        if (req.query.user) {
            criteria.user = req.query.user;
        }

        const photo = await Photo.find(criteria).populate('user', '_id, displayName');

        return res.send(photo)
    }catch (e) {
        return res.status(500).send(e)
   }

});




router.get('/:id',tryAuth, async (req, res) => {
  try {
    const photo = await Photo.findById(req.params.id);
    res.send({photo});
  }catch (e) {
    res.sendStatus(500);
  }
});

router.post('/', auth, upload.single('image'), (req, res) => {
  const photoData = req.body;

  if (req.file) {
      photoData.image = req.file.filename;
  }


    photoData.user = req.user._id;

  const photo = new Photo(photoData);

    photo.save()
    .then(result => res.send(result))
    .catch(error => res.status(400).send(error));
});

router.post('/:id/toggle_removed', auth, async (req, res) => {
  const photo = await Photo.findById(req.params.id);

  if (!photo) {
    return res.sendStatus(404);
  }

    photo.removed = !photo.removed;

  await photo.save();

  res.send(photo);
});

router.delete('/:id', auth, async (req, res) => {
  const success = {message:'Removed'};
  try {
    const photo = await Photo.findById(req.params.id);

    await photo.remove();
    res.sendStatus(200)

  }catch(e)
  {
    res.sendStatus(400)
  }

  return res.send(success)
});


module.exports = router;